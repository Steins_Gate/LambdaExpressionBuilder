﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Gate.ExpressionBuilder.Enums
{
    /// <summary>
    /// 运算符类型
    /// </summary>
    public enum Conditions
    {
        /// <summary>
        /// 不参与查询
        /// </summary>
        Pass = 0,
        /// <summary>
        /// 等于
        /// </summary>
        Equal = 10,
        /// <summary>
        /// 不等于
        /// </summary>
        NotEqual = 11,
        /// <summary>
        /// 大于
        /// </summary>
        GreaterThan = 20,
        /// <summary>
        /// 大于等于
        /// </summary>
        GreaterThanOrEqual = 21,
        /// <summary>
        /// 小于
        /// </summary>
        LessThan = 30,
        /// <summary>
        /// 小于等于
        /// </summary>
        LessThanOrEqual = 31,
        /// <summary>
        /// 包含此值
        /// 列表的话为in
        /// 字符串的话百分号在左右两边
        /// </summary>
        Contains = 40,
        /// <summary>
        /// 以此值开头，百分号在右边
        /// </summary>
        StartsWith = 41,
        /// <summary>
        /// 以此值结尾，百分号在左边边
        /// </summary>
        EndsWith = 42,
        /// <summary>
        /// 不包含此值
        /// 列表的话为 not in
        /// not like 百分号在左右两边
        /// </summary>
        NotContains = 43,
        /// <summary>
        /// 不以此值开头，百分号在右边
        /// </summary>
        NotStartsWith = 44,
        /// <summary>
        /// 不以此值结尾，百分号在左边边
        /// </summary>
        NotEndsWith = 45,
        /// <summary>
        /// 集合包含
        /// </summary>
        ListContains = 51,
        /// <summary>
        /// 集合不包含
        /// </summary>
        ListNotContains = 52

    }
}
