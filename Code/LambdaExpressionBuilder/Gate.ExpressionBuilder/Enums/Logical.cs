﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Gate.ExpressionBuilder.Enums
{
    /// <summary>
    /// 逻辑运算类型
    /// </summary>
    public enum Logical
    {
        /// <summary>
        /// 不参与查询
        /// </summary>
        Pass = 0,
        /// <summary>
        /// 并且
        /// </summary>
        And = 1,
        /// <summary>
        /// 或者
        /// </summary>
        Or = 2
    }
}
