﻿using Gate.ExpressionBuilder.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gate.ExpressionBuilder.DataStructure
{
    /// <summary>
    /// Where条件单体
    /// </summary>
    public class WhereConditionItem
    {
        /// <summary>
        /// 逻辑运算类型,默认情况下，内层为或者
        /// 如果填字段名称，表达式中取属性
        /// 如果填值，表达式中则用值
        /// 二者都填时候，取属性
        /// </summary>
        public Logical LogicalType { get; set; } = Logical.Or;
        /// <summary>
        /// 左值字段名称
        /// </summary>
        public string LColumnName { get; set; }
        /// <summary>
        /// 比较类型
        /// </summary>
        public Conditions ConditionsType = Conditions.Pass;
        /// <summary>
        /// 右字段值名称
        /// </summary>
        public string RColumnName { get; set; }
        /// <summary>
        /// 右字段值
        /// </summary>
        public object RValue { get; set; }
    }
    /// <summary>
    /// where条件
    /// </summary>
    public class WhereCondition
    {
        /// <summary>
        /// 逻辑运算类型,默认情况下，外层是并且
        /// </summary>
        public Logical LogicalType { get; set; } = Logical.And;
        /// <summary>
        /// Where查询项
        /// </summary>
        public List<WhereConditionItem> WhereItems { get; set; }
        /// <summary>
        /// 初始化时会给定一个空的数组
        /// </summary>
        public WhereCondition()
        {
            WhereItems = new List<WhereConditionItem>();
        }
    }
}
