﻿using Chloe;
using Gate.Chloe.ExpressionBuilder.DataStructure;
using Gate.Chloe.ExpressionBuilder.Enum;
using Gate.ExpressionBuilder.DataStructure;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gate.Chloe.ExpressionBuilder
{
    /// <summary>
    /// 表达式生成器
    /// </summary>
    public partial class ChloeExpressionBuilder : Gate.ExpressionBuilder.ExpressionBuilder
    {
        /// <summary>
        /// 返回查询，拼接了where和orderby的
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="queryConditions"></param>
        /// <returns></returns>
        public IQuery<T> ChloeQuery<T>(IQuery<T> query, QueryConditions queryConditions)
        {
            if (queryConditions == null)
                return query;
            query = ChloeQueryWhere<T>(query,queryConditions.WhereConditions);
            query = ChloeQueryOrderBy<T>(query, queryConditions.QueryOrderBies);
            return query;
        }
        /// <summary>
        /// 只拼接OrderBy
        /// </summary>
        /// <typeparam name="T">实体类模型</typeparam>
        /// <param name="query">查询句柄</param>
        /// <param name="queryOrderBies">orderBy内容</param>
        /// <returns>查询器</returns>
        public IQuery<T> ChloeQueryOrderBy<T>(IQuery<T> query, List<QueryOrderBy> queryOrderBies)
        {
            return ChloeQueryOrderBy(query, queryOrderBies, null);
        }
        /// <summary>
        /// 只拼接OrderBy
        /// </summary>
        /// <typeparam name="T">实体类模型</typeparam>
        /// <param name="query">查询句柄</param>
        /// <param name="queryOrderBies">orderBy内容</param>
        /// <param name="rejectOrderByAttribute">不加入排序的字段</param>
        /// <returns>查询器</returns>
        public IQuery<T> ChloeQueryOrderBy<T>(IQuery<T> query, List<QueryOrderBy> queryOrderBies, List<string> rejectOrderByAttribute)
        {
            if (rejectOrderByAttribute == null)
                rejectOrderByAttribute = new List<string>();
            //获得拼接了Orderby的
            if (queryOrderBies != null && queryOrderBies.Count != 0)
            {
                //先处理ASC的排序
                StringBuilder orderByStr = new StringBuilder();
                foreach (var orderBy in queryOrderBies)
                {
                    if (rejectOrderByAttribute.Contains(orderBy.ColumnName))
                    {
                        continue;
                    }
                    if (orderBy.OrderByType == SortingOrder.Pass)
                        continue;
                    orderByStr.AppendFormat("{0} {1},", orderBy.ColumnName, orderBy.OrderByType.ToString());
                }
                if (orderByStr.Length != 0)
                    orderByStr = orderByStr.Remove(orderByStr.Length - 1, 1);
                string orderStr = orderByStr.ToString();

                if (!string.IsNullOrEmpty(orderStr))
                    return query.OrderBy(orderByStr.ToString());
                else
                    return query;
            }
            else
            {
                return query;
            };
        }

        /// <summary>
        /// 只拼接Where
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="query">查询句柄</param>
        /// <param name="queryWheres">Where内容</param>
        /// <returns>查询器</returns>
        public IQuery<T> ChloeQueryWhere<T>(IQuery<T> query, List<WhereCondition> queryWheres)
        {
            return ChloeQueryWhere(query, queryWheres, null);
        }
        /// <summary>
        /// 只拼接Where
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="query">查询句柄</param>
        /// <param name="queryWheres">条件结构</param>
        /// <param name="rejectWheresAttribute">不允许查询的字段</param>
        /// <returns>查询器</returns>
        private IQuery<T> ChloeQueryWhere<T>(IQuery<T> query, List<WhereCondition> queryWheres, List<string> rejectWheresAttribute)
        {
            if (rejectWheresAttribute == null)
                rejectWheresAttribute = new List<string>();

            //移除不允许查询的字段
            if (rejectWheresAttribute != null && rejectWheresAttribute.Count > 0)
            {
                for (int i = 0; i < queryWheres.Count(); i++)
                {
                    if (queryWheres[i].WhereItems != null && queryWheres[i].WhereItems.Count() != 0)
                    {
                        for (int j = 0; j < queryWheres[i].WhereItems.Count(); j++)
                        {
                            if (rejectWheresAttribute.Contains(queryWheres[i].WhereItems[j].LColumnName) || rejectWheresAttribute.Contains(queryWheres[i].WhereItems[j].RColumnName))
                            {
                                queryWheres[i].WhereItems.RemoveAt(j);
                                j--;
                                continue;
                            }
                        }
                    }
                    if (queryWheres[i].WhereItems == null || queryWheres[i].WhereItems.Count() == 0)
                    {
                        queryWheres.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
            }

            var queryWhhere = GetExpression<T>(queryWheres);
            if (queryWhhere != null)
            {
                return query.Where(queryWhhere);
            }
            else
            {
                return query;
            }
        }
    }
}
