﻿using Gate.Chloe.ExpressionBuilder.Enum;
using Gate.ExpressionBuilder.DataStructure;
using System.Collections.Generic;

namespace Gate.Chloe.ExpressionBuilder.DataStructure
{
    /// <summary>
    /// 排序条件
    /// </summary>
    public class QueryOrderBy
    {
        /// <summary>
        /// 字段名称
        /// </summary>
        public string ColumnName { get; set; }
        /// <summary>
        /// 排序类型
        /// </summary>
        public SortingOrder OrderByType { get; set; }
    }
    /// <summary>
    /// 查询体哦阿健
    /// </summary>
    public class QueryConditions
    {
        public List<WhereCondition> WhereConditions { get; set; }
        public List<QueryOrderBy> QueryOrderBies { get; set; }
    }
}
