﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gate.Chloe.ExpressionBuilder.Enum
{
    /// <summary>
    /// 排序类型
    /// </summary>
    public enum SortingOrder
    {
        /// <summary>
        /// 无视排序
        /// </summary>
        Pass = 0,
        /// <summary>
        /// 正序
        /// </summary>
        Asc = 1,
        /// <summary>
        /// 倒序
        /// </summary>
        Desc = 2
    }
}
