using Chloe.SqlServer;
using Gate.Chloe.ExpressionBuilder;
using Gate.Chloe.ExpressionBuilder.DataStructure;
using Gate.Chloe.ExpressionBuilder.Enum;
using Gate.ExpressionBuilder;
using Gate.ExpressionBuilder.DataStructure;
using Gate.ExpressionBuilder.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Test.ExpressionBuilderTest.Modules;

namespace Test.ExpressionBuilderTest
{
    [TestClass]
    public class TestExpressionBuilderTest
    {
        /// <summary>
        /// 获得测试用的数据
        /// </summary>
        /// <returns></returns>
        private List<TestClass> GetTestClasses()
        {
            List<TestClass> testClasses = new List<TestClass>();
            for (int i = 0; i < 1000; i++)
            {
                TestClass testClass = new TestClass()
                {
                    Id = Guid.NewGuid(),
                    Code = i.ToString(),
                    Name = (i + i % 2).ToString(),
                    Num = i,
                    Value = i * i,
                    IsDouble = (i % 2 == 0)
                };
                testClasses.Add(testClass);
            }
            return testClasses;
        }
        /// <summary>
        /// 测试单条件的情况
        /// </summary>
        [TestMethod]
        public void Test1()
        {
            List<TestClass> testClasses = GetTestClasses();
            ExpressionBuilder expressionBuilder = new ExpressionBuilder();
            //Num<=500的，按理有501个
            List<WhereCondition> numLE500WhereConditions = new List<WhereCondition>()
            {
                new WhereCondition()
                {
                    WhereItems = new List<WhereConditionItem>()
                    {
                        new WhereConditionItem()
                        {
                            LColumnName = "Num",
                            RValue = 500,
                            ConditionsType = Conditions.LessThanOrEqual
                        }
                    }
                }
            };
            var numLe500WhereConditionQueryWhere = expressionBuilder.GetExpression<TestClass>(numLE500WhereConditions);
            List<TestClass> numLE500;
            if (numLe500WhereConditionQueryWhere != null)
            {
                numLE500 = testClasses.Where(numLe500WhereConditionQueryWhere.Compile()).ToList();
                if (numLE500.Count != 501)
                    throw new Exception($"按理会有501个,现在有{numLE500.Count}个,数目不对");
            }
            else
                throw new Exception("没法转换为Lambda表达式");
            //chloe ORM测试
            //这个只能目测了

            ChloeExpressionBuilder chloeExpressionBuilder = new ChloeExpressionBuilder();
            QueryConditions queryConditions = new QueryConditions()
            {
                WhereConditions = numLE500WhereConditions,
                QueryOrderBies = new List<QueryOrderBy>()
            };
            MsSqlContext context = new MsSqlContext("");
            var contextQuery = chloeExpressionBuilder.ChloeQuery<TestClass>( context.Query<TestClass>(), queryConditions);
            string sql = contextQuery.ToString();
        }

        /// <summary>
        /// 测试2个单条件的情况
        /// </summary>
        [TestMethod]
        public void Test2()
        {
            List<TestClass> testClasses = GetTestClasses();
            ExpressionBuilder expressionBuilder = new ExpressionBuilder();

            //Num<=100,或者 num>=800的，按理有400个
            List<WhereCondition> numLE100OrGE800WhereConditions = new List<WhereCondition>()
            {
                new WhereCondition()
                {
                    WhereItems = new List<WhereConditionItem>()
                    {
                        new WhereConditionItem()
                        {
                            LColumnName = "Num",
                            RValue = 100,
                            ConditionsType = Conditions.LessThanOrEqual
                        },
                        new WhereConditionItem()
                        {
                            LColumnName = "Code",
                            RValue = 800,
                            ConditionsType = Conditions.GreaterThanOrEqual
                        }
                    }
                }
            };
            var numLE100orGE800WhereConditionQueryWhere = expressionBuilder.GetExpression<TestClass>(numLE100OrGE800WhereConditions);
            List<TestClass> numLE100orGE800;
            if (numLE100orGE800WhereConditionQueryWhere != null)
            {
                numLE100orGE800 = testClasses.Where(numLE100orGE800WhereConditionQueryWhere.Compile()).ToList();
                if (numLE100orGE800.Count != 301)
                    throw new Exception($"按理会有301个,现在有{numLE100orGE800.Count}个,数目不对");
            }
            else
                throw new Exception("没法转换为Lambda表达式");

            ChloeExpressionBuilder chloeExpressionBuilder = new ChloeExpressionBuilder();
            QueryConditions queryConditions = new QueryConditions()
            {
                WhereConditions = numLE100OrGE800WhereConditions,
                QueryOrderBies = new List<QueryOrderBy>()
            };
            MsSqlContext context = new MsSqlContext("");
            var contextQuery = chloeExpressionBuilder.ChloeQuery<TestClass>(context.Query<TestClass>(), queryConditions);
            string sql = contextQuery.ToString();

        }

        /// <summary>
        /// 测试3个条件，先或再且的情况
        /// </summary>
        [TestMethod]
        public void Test3()
        {
            List<TestClass> testClasses = GetTestClasses();
            ExpressionBuilder expressionBuilder = new ExpressionBuilder();

            //(Num<=100,或者 num>=800) 且 IsDouble == treu的，按理有200个
            List<WhereCondition> WhereConditions = new List<WhereCondition>()
            {
                new WhereCondition()
                {
                    WhereItems = new List<WhereConditionItem>()
                    {
                        new WhereConditionItem()
                        {
                            LColumnName = "Num",
                            RValue = 100,
                            ConditionsType = Conditions.LessThanOrEqual
                        },
                        new WhereConditionItem()
                        {
                            LColumnName = "Num",
                            RValue = 800,
                            ConditionsType = Conditions.GreaterThanOrEqual
                        }
                    }
                },
                new WhereCondition()
                {
                    LogicalType = Logical.And,
                    WhereItems = new List<WhereConditionItem>()
                    {
                        new WhereConditionItem()
                        {
                            LColumnName = "IsDouble",
                            RValue = true,
                            ConditionsType = Conditions.Equal
                        }
                    }
                }
            };
            var WhereConditionsQueryWhere = expressionBuilder.GetExpression<TestClass>(WhereConditions);
            List<TestClass> data;
            if (WhereConditionsQueryWhere != null)
            {
                data = testClasses.Where(WhereConditionsQueryWhere.Compile()).ToList();
                if (data.Count != 151)
                    throw new Exception($"按理会有151个,现在有{data.Count}个,数目不对");
            }
            else
                throw new Exception("没法转换为Lambda表达式");


            ChloeExpressionBuilder chloeExpressionBuilder = new ChloeExpressionBuilder();
            QueryConditions queryConditions = new QueryConditions()
            {
                WhereConditions = WhereConditions,
                QueryOrderBies = new List<QueryOrderBy>()
            };
            MsSqlContext context = new MsSqlContext("");
            var contextQuery = chloeExpressionBuilder.ChloeQuery<TestClass>(context.Query<TestClass>(), queryConditions);
            string sql = contextQuery.ToString();
        }

        /// <summary>
        /// 测试包含的情况
        /// </summary>
        [TestMethod]
        public void Test4()
        {
            List<TestClass> testClasses = GetTestClasses();
            ExpressionBuilder expressionBuilder = new ExpressionBuilder();

            //(Num<=100,或者 num>=800) 且 IsDouble == treu的，按理有200个
            List<WhereCondition> WhereConditions = new List<WhereCondition>()
            {
                new WhereCondition()
                {
                    WhereItems = new List<WhereConditionItem>()
                    {
                        new WhereConditionItem()
                        {
                            LColumnName = "Num",
                            RValue = new List<int>{ 1,2,3},
                            ConditionsType = Conditions.ListContains
                        }
                    }
                }
            };
            var WhereConditionsQueryWhere = expressionBuilder.GetExpression<TestClass>(WhereConditions);
            List<TestClass> data;
            if (WhereConditionsQueryWhere != null)
            {
                data = testClasses.Where(WhereConditionsQueryWhere.Compile()).ToList();
                if (data.Count != 3)
                    throw new Exception($"按理会有3个,现在有{data.Count}个,数目不对");
            }
            else
                throw new Exception("没法转换为Lambda表达式");


            ChloeExpressionBuilder chloeExpressionBuilder = new ChloeExpressionBuilder();
            QueryConditions queryConditions = new QueryConditions()
            {
                WhereConditions = WhereConditions,
                QueryOrderBies = new List<QueryOrderBy>()
                {
                    new QueryOrderBy()
                    {
                        ColumnName = "Num",
                        OrderByType = SortingOrder.Desc
                    },
                    new QueryOrderBy()
                    {
                        ColumnName = "Code",
                        OrderByType = SortingOrder.Asc
                    }
                }
            };
            MsSqlContext context = new MsSqlContext("");
            var contextQuery = chloeExpressionBuilder.ChloeQuery<TestClass>(context.Query<TestClass>(), queryConditions);
            string sql = contextQuery.ToString();
        }
    }
}
