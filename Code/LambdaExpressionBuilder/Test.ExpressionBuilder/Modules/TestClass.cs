﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.ExpressionBuilderTest.Modules
{
    /// <summary>
    /// 用于测试的实体类
    /// </summary>
    public class TestClass
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Num { get; set; }
        public double Value { get; set; }
        public bool IsDouble { get; set; }
    }
}
